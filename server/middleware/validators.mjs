import {userSchema, usersSchema} from '../schemas.mjs'

export const validateUser = async (req, res, next) => {
    await userSchema.validateAsync(req.body);
    next()
}
export const validateUsers = async (req, res, next) => {
    await usersSchema.validateAsync(req.body);
    next()
}


