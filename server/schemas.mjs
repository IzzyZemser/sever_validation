import Joi from 'joi'

export const userSchema = Joi.object({
    first_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    last_name: Joi.string()
        .min(3)
        .max(30)
        .required(),
    email: Joi.string()
        .email({ tlds: { allow: false } }),
  
    phone: 
        Joi.string().pattern(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/).required()
  })


export const usersSchema = Joi.array().items(userSchema)