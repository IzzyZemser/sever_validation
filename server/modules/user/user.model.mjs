import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : {
        type: String,
        minlength: [3, "first name too short"],
        maxlegnth: [30, "first name too long"],
        required: true
      },
    last_name   : {
        type: String,
        minlength: [3, "last name too short"],
        maxlegnth: [30, "last name too long"],
        required: [true, "Put in last name please"]
      },
    email       : {
        type: String,
        validate: {
            validator: function(email){
                let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return emailRegex.test(email);
            },
            message: props => `${props.value} not valid email!`
        },
        required: true
      },
    phone       : {
        type: String,
        validate: {
            validator: function(v) {
              return /^\d{3}-\d{3}-\d{4}$/.test(v);
            },
            message: props => `${props.value} is not a valid phone number!`
          },
        required: true
      }
});
  
export default model('user',UserSchema);